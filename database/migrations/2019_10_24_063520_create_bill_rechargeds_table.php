<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBillRechargedsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bill_rechargeds', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('buy_date');
            $table->string('customer_name');
            $table->string('ph_no');
            $table->string('router_no');
            $table->string('month');
            $table->integer('charges');
            $table->string('remark');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bill_rechargeds');
    }
}
