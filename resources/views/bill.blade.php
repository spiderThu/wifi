<!DOCTYPE html>
<html lang="en">
   <head>
      <title> Wifi Broadband Management</title>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="keywords" content="ihost,iHost,domain,hosting,web hosting,email,myanmar hosting,spider"/>
      <link rel="SHORTCUT ICON" href="images/spiderlogo.png">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      <!--===============================================================================================-->   
      <!-- <link rel="icon" type="image/png" href="images/icons/favicon.ico"/> -->
      <!--===============================================================================================-->
      <link rel="stylesheet" type="text/css" href="vendor/bootstrap/css/bootstrap.min.css">
      <!--===============================================================================================-->
      <link rel="stylesheet" type="text/css" href="vendor/animate/animate.css">
      <!--===============================================================================================-->
      <link rel="stylesheet" type="text/css" href="vendor/select2/select2.min.css">
      <!--===============================================================================================-->
      <link rel="stylesheet" type="text/css" href="vendor/perfect-scrollbar/perfect-scrollbar.css">
      <!--===============================================================================================-->
      <link rel="stylesheet" type="text/css" href="css/util.css">
      <link rel="stylesheet" type="text/css" href="css/main.css">
      <!--===============================================================================================-->
   </head>
   <body>
      <div class="button-component">
         <p class="logo-text"><a href="https://www.facebook.com/spidernetworkgroup/" target="_blank" style="text-decoration: none;">
          <img src="images/spiderlogobackground.png" class="logo">
          </a></p>
         <h2 class="wifi"> <a href="{{ route('index') }}">Wifi Broadband Management </a></h2>
         <a href="{{ route('index') }}"><button type="button" class="bill-recharged" style="padding: 3px;">BuyList Page</button></a>

         <!-- export and import -->
         <?php if(auth()->check()): ?> <!-- // do logged in stuff -->
         <table style="float: right;">
            <tr>
               <td colspan="2">Admin Name : {{ Auth::user()->name }}</td>
            </tr>
            <tr>
               <td>
               </td>
               <td><a href="{{ route('logout') }}"><button type="button" class="export">Logout</button></a></td>
            </tr>
         </table>
         <br>
         <table style="float: right;">
            <form action="{{ route('bill-import') }}" method="POST" enctype="multipart/form-data">
            @csrf
            <tr>
               <td colspan="2"><input type="file" name="select_file" class="form-control"></td>
            </tr>
            <tr>
               <td>
                  @if($message = Session::get('success'))
            <div class="alert alert-success alert-block">
             <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ $message }}</strong>
            </div>
            @endif
            
            
            <button type="submit" class="import">Import</button>
            </form>
               </td>
               <td><a href="{{ route('bill-export') }}"><button type="button" class="export">Export</button></a></td>
               
            </tr>
         </table>
         <?php else: ?><!-- // do guest stuff -->
          <table style="float: right;">
            <!-- <tr>
               <td colspan="2">Admin Name : </td>
            </tr> -->
            <tr>
               <td><a href="{{ route('login') }}"><button type="button" class="export">Login</button></a>
               </td>
               <td></td>
            </tr>
         </table>
         <?php endif; ?>
         <!-- export and import -->

         <h2 class="buy-list"><a href="{{ route('bill-recharged') }}">Bill Recharged</a></h2>
         <br>

         <?php if(auth()->check()): ?>
         <form action="{{ route('add-billlist') }}" method="GET" enctype="multipart/form-data">
            @csrf 
            <button type="submit" class="search-button"> Add New Data</button>
         </form>
         <?php endif; ?>

         <form action="{{ route('search-billlist') }}" method="GET" enctype="multipart/form-data">
            @csrf
            <p class="search-component">           
            <input type="text" name="bill" class="search-bar"  placeholder="Search" />
            <button type="submit" class="search-button"> Search </button>
            </p>
         </form>
      </div>
      <div class="container-table100">
         <div class="wrap-table100">
            <h3>Total Count : {{$totals}}</h3>
            <div class="table">
               <div class="row header">
                  <div class="cell">
                     No.
                  </div>
                  <div class="cell">
                     Date
                  </div>
                  <div class="cell">
                     Customer Name
                  </div>
                  <div class="cell">
                     Phone Number
                  </div>
                  <div class="cell">
                     Router Number
                  </div>
                  <?php if(auth()->check()): ?>
                  <div class="cell">
                    Router Type
                  </div>
                  <?php else: ?>
                  <?php endif; ?>
                  <div class="cell">
                     Month
                  </div>
                  <div class="cell">
                     Charges
                  </div>
                  <div class="cell">
                     Expire Date
                  </div>
                  <div class="cell">
                     Remark
                  </div>
               </div>
               <?php if($data): ?>
               @foreach($data as $row)
               <div class="row">
                  <div class="cell" data-title="No.">
                     {{ $row->id }}
                  </div>
                  <div class="cell" data-title="Date">
                     {{ $row->recharged_date }}
                  </div>
                  <div class="cell" data-title="Customer Name">
                    <?php if(auth()->check()): ?>
                     <a href="{{ route('edit-billlist',$row->id) }}">{{ $row->customer_name }}</a>
                     <?php else: ?>
                     {{ $row->customer_name }}
                    <?php endif; ?>
                  </div>
                  <div class="cell" data-title="Phone Number">
                     {{ $row->ph_no }}
                  </div>
                  <div class="cell" data-title="Router Number">
                     {{ $row->router_no }}
                  </div>
                  <?php if(auth()->check()): ?>
                  <div class="cell" data-title="Router Type">
                     {{ $row->router_type }}
                  </div>
                  <?php else: ?>
                      
                  <?php endif; ?>
                  <div class="cell" data-title="Month">
                     {{ $row->month }}
                  </div>
                  <div class="cell" data-title="Charges">
                     <?= number_format($row->charges);?>
                  </div>
                  <div class="cell" data-title="Expire Date">
                    <?php 
                      $rechargedDate = $row->recharged_date;
                      $mm = $row->month;
                      $rtype = $row->router_type;
                      if($mm == 3){
                        $expire_date = date('Y-m-d', strtotime($rechargedDate. ' + 90 days'));
                      }
                      elseif($mm == 1){
                        $expire_date = date('Y-m-d', strtotime($rechargedDate. ' + 30 days'));
                      }
                      elseif($rtype == "MyTel"){
                        $expire_date = date('Y-m-d', strtotime($rechargedDate. ' + 30 days'));
                      }
                      echo $expire_date;
                    ?>
                  </div>
                  <div class="cell" data-title="Remark">
                     {{ $row->remark }}
                  </div>
               </div>
               @endforeach
               <?php endif; ?>

               <?php if($results): ?>
               @foreach($results as $row)
               <div class="row">
                  <div class="cell" data-title="No.">
                     {{ $row->id }}
                  </div>
                  <div class="cell" data-title="Date">
                     {{ $row->recharged_date }}
                  </div>
                  <div class="cell" data-title="Customer Name">
                     <?php if(auth()->check()): ?>
                     <a href="{{ route('edit-billlist',$row->id) }}">{{ $row->customer_name }}</a>
                     <?php else: ?>
                     {{ $row->customer_name }}
                    <?php endif; ?>
                  </div>
                  <div class="cell" data-title="Phone Number">
                     {{ $row->ph_no }}
                  </div>
                  <div class="cell" data-title="Router Number">
                     {{ $row->router_no }}
                  </div>
                  <?php if(auth()->check()): ?>
                  <div class="cell" data-title="Router Type">
                     {{ $row->router_type }}
                  </div>
                  <?php else: ?>
                      
                  <?php endif; ?>
                  <div class="cell" data-title="Month">
                     {{ $row->month }}
                  </div>
                  <div class="cell" data-title="Charges">
                     <?= number_format($row->charges);?>
                  </div>
                  <div class="cell" data-title="Expire Date">
                    <?php 
                      $rechargedDate = $row->recharged_date;
                      $mm = $row->month;
                      $rtype = $row->router_type;
                      if($mm == 3){
                        $expire_date = date('Y-m-d', strtotime($rechargedDate. ' + 90 days'));
                      }
                      elseif($mm == 1){
                        $expire_date = date('Y-m-d', strtotime($rechargedDate. ' + 30 days'));
                      }
                      elseif($rtype == "MyTel"){
                        $expire_date = date('Y-m-d', strtotime($rechargedDate. ' + 30 days'));
                      }
                      echo $expire_date;
                    ?>
                  </div>
                  <div class="cell" data-title="Remark">
                     {{ $row->remark }}
                  </div>
               </div>
               @endforeach
               <?php endif; ?>
            </div>
         </div>
         <!-- pagination -->
         <?php if($data): ?>
         @if ($data->hasPages())
         <nav>
            <ul class="pagination">
            {{-- Previous Page Link --}}
            @if ($data->onFirstPage())
                <li class="page-item disabled" aria-disabled="true">
                    <span class="page-link">@lang('pagination.previous')</span>
                </li>
            @else
                <li class="page-item">
                    <a class="page-link" href="{{ $data->previousPageUrl() }}" rel="prev">@lang('pagination.previous')</a>
                </li>
            @endif

            {{-- Next Page Link --}}
            @if ($data->hasMorePages())
                <li class="page-item">
                    <a class="page-link" href="{{ $data->nextPageUrl() }}" rel="next">@lang('pagination.next')</a>
                </li>
            @else
                <li class="page-item disabled" aria-disabled="true">
                    <span class="page-link">@lang('pagination.next')</span>
                </li>
            @endif
            </ul>
         </nav>
         @endif
         <?php endif; ?>

         <?php if($results): ?>
         @if ($results->hasPages())
         <nav>
            <ul class="pagination">
            {{-- Previous Page Link --}}
            @if ($results->onFirstPage())
                <li class="page-item disabled" aria-disabled="true">
                    <span class="page-link">@lang('pagination.previous')</span>
                </li>
            @else
                <li class="page-item">
                    <a class="page-link" href="{{ $results->previousPageUrl() }}" rel="prev">@lang('pagination.previous')</a>
                </li>
            @endif

            {{-- Next Page Link --}}
            @if ($results->hasMorePages())
                <li class="page-item">
                    <a class="page-link" href="{{ $results->nextPageUrl() }}" rel="next">@lang('pagination.next')</a>
                </li>
            @else
                <li class="page-item disabled" aria-disabled="true">
                    <span class="page-link">@lang('pagination.next')</span>
                </li>
            @endif
            </ul>
         </nav>
         @endif
         <?php endif; ?>
         <!-- pagination -->
          
      </div>
      <a href="#top">
            <!-- ⏏️ -->
            <img src="images/up.png" style="float: right;">
      </a>
      <!--===============================================================================================-->   
      <script src="vendor/jquery/jquery-3.2.1.min.js"></script>
      <!--===============================================================================================-->
      <script src="vendor/bootstrap/js/popper.js"></script>
      <script src="vendor/bootstrap/js/bootstrap.min.js"></script>
      <!--===============================================================================================-->
      <script src="vendor/select2/select2.min.js"></script>
      <!--===============================================================================================-->
      <script src="js/main.js"></script>
   </body>
</html>

