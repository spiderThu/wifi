<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Colorlib Templates">
    <meta name="author" content="Colorlib">
    <meta name="keywords" content="Colorlib Templates">

    <!-- Title Page-->
    <title>Wifi Broadband Management</title>

    <!-- Icons font CSS-->
    <link href="../vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">
    <link href="../vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <!-- Font special for pages-->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i" rel="stylesheet">

    <!-- Vendor CSS-->
    <link href="../vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="../vendor/datepicker/daterangepicker.css" rel="stylesheet" media="all">

    <!-- Main CSS-->
    <link href="../css/main2.css" rel="stylesheet" media="all">
    <script type="text/javascript">
        function sequence(){
            var tt = document.getElementById('field1').value;
            var ss= document.getElementById('field2').value;
            var rr= document.getElementById('field3').value;

            if(Number(ss) == '3'){
                //alert("3 hey");
                var date = new Date(tt);
                var newdate = new Date(date);

                newdate.setDate(newdate.getDate() + 90);
                
                var dd = newdate.getDate();
                var mm = newdate.getMonth() + 1;
                var y = newdate.getFullYear();

                var someFormattedDate = mm + '/' + dd + '/' + y;
            }
            if(Number(ss) == '1'){
                //alert("1 hey");
                var date = new Date(tt);
                var newdate = new Date(date);

                newdate.setDate(newdate.getDate() + 30);
                
                var dd = newdate.getDate();
                var mm = newdate.getMonth() + 1;
                var y = newdate.getFullYear();

                var someFormattedDate = mm + '/' + dd + '/' + y;
            }
            if(rr == 'MyTel'){
                var date = new Date(tt);
                var newdate = new Date(date);

                newdate.setDate(newdate.getDate() + 30);
                
                var dd = newdate.getDate();
                var mm = newdate.getMonth() + 1;
                var y = newdate.getFullYear();

                var someFormattedDate = mm + '/' + dd + '/' + y;
            }
        
            document.getElementById('expDate').value = someFormattedDate; 
         }
    </script>
</head>

<body>
    <div class="page-wrapper bg-gra-03 p-t-45 p-b-50">
        <div class="wrapper wrapper--w790">
            <div class="card card-5">
                <div class="card-heading">
                    <h2 class="title">BillRecharged Edit Data</h2>
                </div>
                <form action="{{ route('update-billlist',$bills->id) }}" method="POST">
                @csrf
                <div class="card-body">
                    <form method="POST">
                        <div class="form-row m-b-55">
                            <div class="name">Buy Date</div>
                            <div class="value">
                                <div class="row row-space">
                                    <div class="col-2">
                                        <div class="input-group-desc">
                                            <input class="input--style-5" type="date" value="{{ $bills->recharged_date }}" name="recharged_date" id="field1" required="required">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="name">Customer Name</div>
                            <div class="value">
                                <div class="input-group">
                                    <input class="input--style-5" type="text" name="customer_name" value="{{ $bills->customer_name }}" required="required">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="name">Ph Number</div>
                            <div class="value">
                                <div class="input-group">
                                    <input class="input--style-5" type="text" name="ph_no" value="{{ $bills->ph_no }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="name">Router Number</div>
                            <div class="value">
                                <div class="input-group">
                                    <input class="input--style-5" type="text" name="router_no" value="{{ $bills->router_no }}">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="name">Router Type</div>
                            <div class="value">
                                <div class="input-group">
                                    <input class="input--style-5" type="text" name="router_type" value="{{ $bills->router_type }}" id="field3" onchange="sequence()">
                                </div>
                            </div>
                        </div>
                        <div class="form-row m-b-55">
                            <div class="name">Month</div>
                            <div class="value">
                                <div class="row row-refine">
                                    <div class="col-3">
                                        <div class="input-group-desc">
                                            <input class="input--style-5" type="text" name="month" value="{{ $bills->month }}" id="field2" onchange="sequence()">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row m-b-55">
                            <div class="name">Charges</div>
                            <div class="value">
                                <div class="row row-refine">
                                    <div class="col-3">
                                        <div class="input-group-desc">
                                            <input class="input--style-5" type="text" name="charges" value="{{ $bills->charges }}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="name">Expire Date</div>
                            <div class="value">
                                <div class="input-group">
                                    <?php $bb = $bills->expire_date; 
                                    //echo $bb;exit();
                                    if($bb == "0000-00-00"){
                                        $billDate = $bills->recharged_date;
                                        $mm = $bills->month;
                                        $rtype = $bills->router_type;
                                        if($mm == 3){
                                            $expvalue = date('Y-m-d', strtotime($billDate. ' + 90 days'));
                                        }
                                        elseif($mm == 1){
                                            $expvalue = date('Y-m-d', strtotime($billDate. ' + 30 days'));
                                        }
                                        elseif($rtype == "MyTel"){
                                            $expvalue = date('Y-m-d', strtotime($billDate. ' + 30 days'));
                                        }
                                        else{
                                            $expvalue = $bb;
                                        }
                                    }
                                    else{
                                        $expvalue = $bb;
                                    }
                                    ?>
                                    <input class="input--style-5" type="text" name="expire_date" value="<?= $expvalue; ?>" id="expDate">
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="name">Remark</div>
                            <div class="value">
                                <div class="input-group">
                                    <input class="input--style-5" type="text" name="remark" value="{{ $bills->remark }}">
                                </div>
                            </div>
                        </div>
                        <div>
                            <button class="btn btn--radius-2 btn--red" type="submit">Update</button>
                        </div>
                    </form>
                    <br><br>
                    <a href="{{ route('bill-recharged') }}" class="ForgetPwd" value="Login">This page is not for guest,Go Back👈</a>
                </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Jquery JS-->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <!-- Vendor JS-->
    <script src="../vendor/select2/select2.min.js"></script>
    <script src="../vendor/datepicker/moment.min.js"></script>
    <script src="../vendor/datepicker/daterangepicker.js"></script>

    <!-- Main JS-->
    <script src="../js/global.js"></script>

</body><!-- This templates was made by Colorlib (https://colorlib.com) -->

</html>
<!-- end document-->