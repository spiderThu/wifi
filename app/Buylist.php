<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buylist extends Model
{
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'buy_date', 'customer_name', 'ph_no','router_no', 'mme_no', 'charges','remark','updated_at','created_at'
    ];

    
}
