<?php

namespace App\Imports;

use App\Buylist;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use DateTime;

class BuylistsImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new Buylist([
            'buy_date' => date('Y-m-d', strtotime($row['buy_date'])),
            'customer_name' => $row['customer_name'],
            'ph_no'  => $row['ph_no'],
            'router_no'   => $row['router_no'],
            'mme_no'   => $row['mme_no'],
            'charges'    => $row['charges'],
            'remark'  => $row['remark'],
        ]);
    }
}
