<?php

namespace App\Imports;

use App\BillRecharged;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class BillRechargedImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new BillRecharged([
            'buy_date' => date('Y-m-d', strtotime($row['buy_date'])),
            'customer_name' => $row['customer_name'],
            'ph_no'  => $row['ph_no'],
            'router_no'   => $row['router_no'],
            'month'   => $row['month'],
            'charges'    => $row['charges'],
            'remark'  => $row['remark'],
        ]);
    }
}
