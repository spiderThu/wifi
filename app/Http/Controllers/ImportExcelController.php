<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Buylist;
use App\BillRecharged;
use App\Imports\BuylistsImport;
use App\Imports\BillRechargedImport;
use App\Exports\BuylistsExport;
use App\Exports\BillRechargedExport;
use Maatwebsite\Excel\Facades\Excel;

class ImportExcelController extends Controller
{
    // function import(Request $request)
    // {
    //   $this->validate($request, [
    //     'select_file'  => 'required|mimes:xls,xlsx'
    //    ]);

    //  $path = $request->file('select_file')->getRealPath();

    //  //$data = Excel::load($path)->get();
    //  $data = Excel::load($path)->get();

    //  if($data->count() > 0)
    //  {
    //   foreach($data->toArray() as $key => $value)
    //   {
    //    foreach($value as $row)
    //    {
    //     $insert_data[] = array(
    //      'Date' => $row['date'],
    //      'CustomerName' => $row['customer_name'],
    //      'PhoneNumber'  => $row['ph_no'],
    //      'RouterNumber'   => $row['router_no'],
    //      'MMENumber'   => $row['mme_no'],
    //      'Charges'    => $row['charges'],
    //      'Remark'  => $row['remark']
    //     );
    //    }
    //   }

    //   if(!empty($insert_data))
    //   {
    //    DB::table('tb_buylists')->insert($insert_data);
    //   }
    //  }
    //  return back()->with('success', 'Excel Data Imported successfully.');
    // }
    public function BuyImport(Request $request) {
      //dd($request->all());exit();
      
      Excel::import(new BuylistsImport(), request()->file('select_file')); 
      //$result = Buylist:all();
      return back()->with('success', 'Insert Record Successfuly');
      // return redirect()->back()->with('success', 'Insert Record Successfuly')->withInput('result');
      //dd($result);exit();
      //return view('index', $result);

    }
    public function BillImport(Request $request) {
      //dd($request->all());exit();
      
      Excel::import(new BillRechargedImport(), request()->file('select_file')); 
      //$result = Buylist:all();
      return back()->with('success', 'Insert Record Successfuly');
      // return redirect()->back()->with('success', 'Insert Record Successfuly')->withInput('result');
      //dd($result);exit();
      //return view('index', $result);

    }
    
    public function BuyExport() 
    {
        return Excel::download(new BuylistsExport, 'buy-list.xlsx');
    }
    public function BillExport() 
    {
        return Excel::download(new BillRechargedExport, 'bill-recharged.xlsx');
    }
}