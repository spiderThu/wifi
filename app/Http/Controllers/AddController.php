<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Buylist;
use App\BillRecharged;

class AddController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addBuyList()
    {
        return view('add_buylist');
    }

    public function insertBuyList(Request $request)
    {
        $buys = new Buylist();
        // print_r($request->all());exit();
 
        $buys->buy_date = request('buy_date');
        $buys->customer_name = request('customer_name');
        $buys->ph_no = request('ph_no');
        $buys->router_no = request('router_no');
        $buys->router_type = request('router_type');

        $expdate = date("Y-m-d", strtotime(request('expire_date')));

        $buys->expire_date = $expdate;
        $buys->charges = request('charges');
        $buys->remark = request('remark');
 
        $buys->save();
 
        return redirect('/');
    }

    public function editBuyList($id)
    {   
        $buys = Buylist::findOrFail($id);
        //dd($buys);exit();
        return view('edit_buylist',compact('buys'));
    }

    public function updateBuyList(Request $request, $id)
    {   
        $buys = Buylist::findOrFail($id);
 
        $buys->buy_date = request('buy_date');
        $buys->customer_name = request('customer_name');
        $buys->ph_no = request('ph_no');
        $buys->router_no = request('router_no');
        $buys->router_type = request('router_type');

        $expdate = date("Y-m-d", strtotime(request('expire_date')));
        $buys->expire_date = $expdate;

        $buys->charges = request('charges');
        $buys->remark = request('remark');

        $buys->save();
 
        return redirect('/');
    }
    
    public function addBillList()
    {
        return view('add_billlist');
    }

    public function insertBillList(Request $request)
    {
        $bills = new BillRecharged();
 
        $bills->recharged_date = request('recharged_date');
        $bills->customer_name = request('customer_name');
        $bills->ph_no = request('ph_no');
        $bills->router_no = request('router_no');
        $bills->router_type = request('router_type');
        $bills->month = request('month');
        $bills->charges = request('charges');

        $expdate = date("Y-m-d", strtotime(request('expire_date')));
        $bills->expire_date = $expdate;

        $bills->remark = request('remark');
 
        $bills->save();
 
        return redirect('/bill-recharged');
    }

    public function editBillList($id)
    {   
        $bills = BillRecharged::findOrFail($id);
        //dd($buys);exit();
        return view('edit_billlist',compact('bills'));
    }

    public function updateBillList(Request $request, $id)
    {   
        $bills = BillRecharged::findOrFail($id);
 
        $bills->recharged_date = request('recharged_date');
        $bills->customer_name = request('customer_name');
        $bills->ph_no = request('ph_no');
        $bills->router_no = request('router_no');
        $bills->router_type = request('router_type');
        $bills->month = request('month');
        $bills->charges = request('charges');
        $expdate = date("Y-m-d", strtotime(request('expire_date')));
        $bills->expire_date = $expdate;
        $bills->remark = request('remark');

        $bills->save();
 
        return redirect('/bill-recharged');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
