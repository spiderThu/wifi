<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Buylist;
use App\BillRecharged;
use Maatwebsite\Excel\Facades\Excel;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $data = DB::table('buylists')->orderBy('buy_date', 'DESC')->paginate(10);
        $results = '';
        $totals = Buylist::count();
        return view('index', compact('data','results','totals'));
    }

    public function bill()
    {   
        $data = DB::table('bill_rechargeds')->orderBy('recharged_date', 'DESC')->paginate(10);
        $results = '';
        $totals = BillRecharged::count();
        return view('bill', compact('data','results','totals'));
    }

    public function logout () {
        //logout user
        auth()->logout();
        // redirect to homepage
        return redirect()->action('HomeController@index');
    }
}
