<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Buylist;
use App\BillRecharged;
use DB;

class SearchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    public function searchBuyList(Request $request){
        $data = '';
        $searchdata = $request->buy;
        if($searchdata){
            $results = DB::table('buylists')
                ->where('customer_name', 'LIKE', '%'.$searchdata.'%')
                ->orWhere('router_no', 'LIKE', '%'.$searchdata.'%')
                ->orWhere('ph_no', 'LIKE', '%'.$searchdata.'%')
                ->orWhere('buy_date', 'LIKE', '%'.$searchdata.'%')
                ->orderBy('buy_date', 'DESC')->paginate(10);
            //dd($results);exit();
            $totals = DB::table('buylists')
                ->where('customer_name', 'LIKE', '%'.$searchdata.'%')
                ->orWhere('router_no', 'LIKE', '%'.$searchdata.'%')
                ->orWhere('ph_no', 'LIKE', '%'.$searchdata.'%')
                ->orWhere('buy_date', 'LIKE', '%'.$searchdata.'%')
                ->orderBy('buy_date', 'DESC')->count();
            return view('index', compact('results','data','totals'));    
        }
        else{
            $results = '';
            $totals = '';
            return view('index', compact('results','data','totals'));  
        }
    }

    public function searchBillList(Request $request){
        $data = '';
        $searchdata = $request->bill;
        if($searchdata){
            $results = DB::table('bill_rechargeds')
                ->where('customer_name', 'LIKE', '%'.$searchdata.'%')
                ->orWhere('router_no', 'LIKE', '%'.$searchdata.'%')
                ->orWhere('ph_no', 'LIKE', '%'.$searchdata.'%')
                ->orWhere('recharged_date', 'LIKE', '%'.$searchdata.'%')
                ->orderBy('recharged_date', 'DESC')->paginate(10);
            //dd($results);exit();
            $totals = DB::table('bill_rechargeds')
                ->where('customer_name', 'LIKE', '%'.$searchdata.'%')
                ->orWhere('router_no', 'LIKE', '%'.$searchdata.'%')
                ->orWhere('ph_no', 'LIKE', '%'.$searchdata.'%')
                ->orWhere('recharged_date', 'LIKE', '%'.$searchdata.'%')
                ->orderBy('recharged_date', 'DESC')->count();
            return view('bill', compact('results','data','totals'));    
        }
        else{
            $results = '';
            $totals = '';
            return view('bill', compact('results','data','totals'));  
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
