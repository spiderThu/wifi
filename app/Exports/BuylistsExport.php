<?php

namespace App\Exports;

use App\Buylist;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class BuylistsExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Buylist::all();
    }
    public function headings(): array
    {
        return [
            'No',
            'Date',
            'Customer Name',
            'Ph No',
            'Router No',
            'MME No',
            'Charges',
            'Remark',
            'Created_At',
            'Updated_At',
        ];
    }
}
