<?php

namespace App\Exports;

use App\BillRecharged;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class BillRechargedExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return BillRecharged::all();
    }

    public function headings(): array
    {
        return [
            'No',
            'Date',
            'Customer Name',
            'Ph No',
            'Router No',
            'Month',
            'Charges',
            'Remark',
            'Created_At',
            'Updated_At',
        ];
    }
}
