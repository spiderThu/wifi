<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillRecharged extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'recharged_date', 'customer_name', 'ph_no','router_no', 'month', 'charges','remark','updated_at','created_at'
    ];
}
