<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', 'HomeController@index')->name('index');
Route::post('buy-import', 'ImportExcelController@BuyImport')->name('buy-import');
Route::get('/bill-recharged', 'HomeController@bill')->name('bill-recharged');
Route::post('bill-import', 'ImportExcelController@BillImport')->name('bill-import');

Route::get('/buy-export', 'ImportExcelController@BuyExport')->name('buy-export');
Route::get('/bill-export', 'ImportExcelController@BillExport')->name('bill-export');

Route::get('/search-buylist', 'SearchController@searchBuyList')->name('search-buylist');
Route::get('/search-billlist', 'SearchController@searchBillList')->name('search-billlist');


Auth::routes();
Route::get('/logout', 'HomeController@logout');

Route::get('/add-buylist', 'AddController@addBuyList')->name('add-buylist');
Route::post('/insert-buylist', 'AddController@insertBuyList')->name('insert-buylist');
Route::get('/edit-buylist/{id}', 'AddController@editBuyList')->name('edit-buylist');
Route::post('/update-buylist/{id}', 'AddController@updateBuyList')->name('update-buylist');
// Route::get('/home', 'HomeController@index')->name('home');

Route::get('/add-billlist', 'AddController@addBillList')->name('add-billlist');
Route::post('/insert-billlist', 'AddController@insertBillList')->name('insert-billlist');
Route::get('/edit-billlist/{id}', 'AddController@editBillList')->name('edit-billlist');
Route::post('/update-billlist/{id}', 'AddController@updateBillList')->name('update-billlist');